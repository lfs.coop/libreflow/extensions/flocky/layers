import os
import glob
import shutil
import fileseq
import subprocess
import concurrent.futures
from kabaret import flow
from libreflow.utils.flow.extension_manager import classqualname
from libreflow.baseflow.file import FolderAvailableRevisionName


class NormalizePNG(flow.Action):
    '''
    TODO: Write description
    '''

    ICON = ('icons.gui', 'cog-wheel-silhouette')

    _folder = flow.Parent()

    revision_name = flow.Param(None, FolderAvailableRevisionName).watched()

    def __init__(self, parent, name):
        super(NormalizePNG, self).__init__(parent, name)
        self.site_env = self.root().project().get_current_site().site_environment
        self.exec_path = None
        self.non_rgb_images = []
        self.resolution = None
        self.colorspace = None

    def needs_dialog(self):
        if self.site_env.has_mapped_name('IMAGEMAGICK_EXEC_PATH'):
            self.exec_path = self.site_env['IMAGEMAGICK_EXEC_PATH'].value.get()
            self.message.set('<h2>Normalize PNG</h2>')
            self.revision_name.revert_to_default()
        else:
            self.message.set('<h2>Normalize PNG</h2>ImageMagick path is not configured')
        return True
    
    def get_buttons(self):
        if not self.site_env.has_mapped_name('IMAGEMAGICK_EXEC_PATH'):
            return ['Cancel']
        else:
            return ['Normalize', 'Cancel']

    def child_value_changed(self, child_value):
        if child_value is self.revision_name:
            source_path = self._folder.get_revision(self.revision_name.get()).get_path()
            backup_dir = source_path+'/BACKUP'
            if os.path.exists(backup_dir):
                self.message.set('<h2>Normalize PNG</h2><font color=#D66700>This revision has been already normalized. Backup folder detected.</font>')

    def run(self, button):
        if button == 'Cancel':
            return

        source_path = self._folder.get_revision(self.revision_name.get()).get_path()

        layers = glob.glob(source_path+'/*/', recursive=True)
        for layer_dir in layers:
            self.non_rgb_images = []
            self.resolution = None
            self.colorspace = None

            layer_dir_name = os.path.basename(layer_dir[:-1])
            if layer_dir_name == 'BACKUP':
                continue
            print(f'Normalize PNG: checking {layer_dir_name}')
            layer_name = os.path.basename(os.listdir(layer_dir)[0][:-9])
            try:
                file_sequence = fileseq.findSequenceOnDisk(layer_dir+f'{layer_name}-@@@@.png')
            except fileseq.exceptions.FileSeqException:
                print(f'Normalize PNG: {layer_name} not found, ignore it')
                continue
            frame_set = file_sequence.frameSet()

            with concurrent.futures.ThreadPoolExecutor() as executor:
                images_check = []
                for i, image_number in enumerate(frame_set):
                    image_source_path = file_sequence.frame(image_number)
                    image_source_name = os.path.basename(image_source_path)
                    images_check.append(executor.submit(self.check_image_metadata, image_source_path, image_source_name))
                
            print('Normalize PNG: checking complete')

            if self.non_rgb_images:
                backup_dir = source_path+'/BACKUP'
                if not os.path.exists(backup_dir):
                    os.makedirs(backup_dir)
                
                backup_layer_dir = backup_dir+f'/{layer_dir_name}'
                os.makedirs(backup_layer_dir)

                with concurrent.futures.ThreadPoolExecutor() as executor:
                    images_create = []
                    for i, non_rgb_image_path in enumerate(self.non_rgb_images):
                        image_name = os.path.basename(non_rgb_image_path)
                        shutil.move(non_rgb_image_path, backup_layer_dir+f'/{image_name}')
                        images_create.append(executor.submit(self.create_image, non_rgb_image_path, image_name))
                
                print(f'Normalize PNG: conversion complete - {image_name}')
    
    def check_image_metadata(self, image_source_path, image_source_name):
        convert_args = [self.exec_path]
        convert_args.extend(['identify', image_source_path])
        
        metadata = subprocess.check_output(convert_args, shell=True).decode()
        metadata = metadata.split(' ')

        colorspace = metadata[5]

        if colorspace != 'sRGB':
            self.non_rgb_images.append(image_source_path)
            print(f'Normalize PNG: incorrect image found - {image_source_name}')
        else:
            if self.resolution is None:
                self.resolution = metadata[2]
                self.colorspace = colorspace

    def create_image(self, output_path, image_name):
        convert_args = [self.exec_path]
        convert_args.extend([
            'convert',
            '-size',
            self.resolution,
            '-colorspace',
            self.colorspace,
            'xc:none',
            f'png32:{output_path}'
        ])

        convert = subprocess.run(convert_args, shell=True)
        print(f'Normalize PNG: image converted - {image_name}')


def get_handlers():
    return [
        ("^/flocky/films/[^/]+/sequences/[^/]+/shots/[^/]+/tasks/[^/]+/files/layers$", "normalize_png", classqualname(NormalizePNG), {'label': 'Normalize PNG'})
    ]